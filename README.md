# Woodpecker Jsonnet addon

[Woodpecker addon](https://woodpecker-ci.org/docs/administration/addons/overview) that allows you to use Jsonnet config files.

Please note that this does not work with private repositories yet.
