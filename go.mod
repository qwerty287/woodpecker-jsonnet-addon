module codeberg.org/qwerty287/woodpecker-jsonnet-addon

go 1.21

require (
	github.com/google/go-jsonnet v0.20.0
	github.com/rs/zerolog v1.31.0
	go.woodpecker-ci.org/woodpecker/v2 v2.1.1
)

require (
	github.com/go-ap/httpsig v0.0.0-20221203064646-3647b4d88fdf // indirect
	github.com/jellydator/ttlcache/v3 v3.1.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/robfig/cron v1.2.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	sigs.k8s.io/yaml v1.4.0 // indirect
)
