package main

import (
	"context"
	"errors"
	"strings"

	"github.com/google/go-jsonnet"
	"github.com/rs/zerolog"
	"go.woodpecker-ci.org/woodpecker/v2/server"
	forgeTypes "go.woodpecker-ci.org/woodpecker/v2/server/forge/types"
	"go.woodpecker-ci.org/woodpecker/v2/server/model"
	"go.woodpecker-ci.org/woodpecker/v2/server/plugins/config"
	addonTypes "go.woodpecker-ci.org/woodpecker/v2/shared/addon/types"
	"go.woodpecker-ci.org/woodpecker/v2/shared/constant"
)

var Type = addonTypes.TypeConfigService

func Addon(_ zerolog.Logger) (config.Extension, error) {
	return &configExtension{
		fileExtension: ".jsonnet",
		evalFunc: func(file *forgeTypes.FileMeta) (*forgeTypes.FileMeta, error) {
			vm := jsonnet.MakeVM()
			jsonData, err := vm.EvaluateAnonymousSnippet(file.Name, string(file.Data))
			if err != nil {
				return nil, err
			}
			return &forgeTypes.FileMeta{
				Name: file.Name,
				Data: []byte(jsonData),
			}, nil
		},
	}, nil
}

type configExtension struct {
	fileExtension string
	evalFunc      func(meta *forgeTypes.FileMeta) (*forgeTypes.FileMeta, error)
}

func (c *configExtension) FetchConfig(ctx context.Context, repo *model.Repo, pipeline *model.Pipeline, currentFileMeta []*forgeTypes.FileMeta, _ *model.Netrc) ([]*forgeTypes.FileMeta, bool, error) {
	if strings.HasSuffix(repo.Config, "/") {
		// folders are not loaded completely (only YAML), we must fetch the additional files separately
		metas, err := c.listDir(ctx, repo, pipeline)
		return append(currentFileMeta, metas...), false, err
	} else if strings.HasSuffix(repo.Config, c.fileExtension) {
		// a single file processed by addon
		if len(currentFileMeta) != 1 {
			return nil, false, errors.New("meta list must contain exactly one item for single-file configs")
		}
		meta, err := c.evalFunc(currentFileMeta[0])
		if err != nil {
			return nil, false, err
		}
		return []*forgeTypes.FileMeta{meta}, false, nil
	} else if repo.Config == "" && len(currentFileMeta) > 0 {
		// uses default config, check if a folder was used
		for _, conf := range constant.DefaultConfigOrder {
			if strings.HasSuffix(conf, "/") && strings.HasPrefix(currentFileMeta[0].Name, conf) {
				// a dir from defaults was used -> list this too
				metas, err := c.listDir(ctx, repo, pipeline)
				return append(currentFileMeta, metas...), false, err
			}
		}
	}
	// no config supported by this addon, keep old
	return nil, true, nil
}

func (c *configExtension) listDir(ctx context.Context, repo *model.Repo, pipeline *model.Pipeline) ([]*forgeTypes.FileMeta, error) {
	// config is a folder
	// TODO get user instead of using placeholder
	files, err := server.Config.Services.Forge.Dir(ctx, &model.User{}, repo, pipeline, strings.TrimSuffix(repo.Config, "/"))
	// if folder is not supported we will get a "Not implemented" error and continue
	if err != nil {
		return nil, err
	}
	var res []*forgeTypes.FileMeta

	for _, file := range files {
		if strings.HasSuffix(file.Name, c.fileExtension) {
			meta, err := c.evalFunc(file)
			if err != nil {
				return nil, err
			}
			res = append(res, meta)
		}
	}

	return res, nil
}
